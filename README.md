![MQTTnet][2]

# README

*MQTT Examples*

1. **Packages Dependencies**

    | Package Name | Version |
    | ------------ | ------- |
    | MQTTnet | [2.8.2][1]
    | Microsoft .NET Framework | 4.5.2 |

2. **MQTT Server**
    1. [Apache Apollo 1.7.1](http://activemq.apache.org/apollo/index.html)
    2. [EMQTT 2.3.11](http://www.emqtt.com/downloads/2318)



[1]: https://www.nuget.org/packages/MQTTnet/2.8.2
[2]: https://raw.githubusercontent.com/chkr1011/MQTTnet/master/Images/Logo_128x128.png
[3]: http://www.emqtt.com/downloads/2318