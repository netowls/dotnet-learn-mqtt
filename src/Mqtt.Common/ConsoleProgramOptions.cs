﻿namespace NetOwls.Tutorials.Mqtt.Common
{
    using System.Diagnostics;

    /// <summary>
    ///     控制台程序设置。
    /// </summary>
    public static class ConsoleProgramOptions
    {
        /// <summary>
        ///     配置控制台程序。
        /// </summary>
        public static void Configure()
        {
            Trace.Listeners.Add(new ConsoleTraceListener());
        }
    }
}