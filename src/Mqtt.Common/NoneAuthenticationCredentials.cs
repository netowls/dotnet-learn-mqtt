﻿namespace NetOwls.Tutorials.Mqtt.Common
{
    using System;

    /// <summary>
    ///     提供了无需身份认证的 MQTT 服务连接凭据。
    ///     <para>
    ///         密闭的，不可从此类型继承。
    ///     </para>
    /// </summary>
    /// <seealso>
    ///     <cref>IMqttConnectionCredentials</cref>
    /// </seealso>
    /// <remarks>
    ///     密闭的，不可从此类型继承。
    /// </remarks>
    public sealed class NoneAuthenticationCredentials : IMqttConnectionCredentials
    {
        /// <summary>
        ///     设置或获取登录口令。
        /// </summary>
        [Obsolete("尚未实现此方法。", true)]
        public string Password
        {
            get { throw new NotImplementedException(); }

            set { throw new NotImplementedException(); }
        }

        /// <summary>
        ///     设置或获取用户名。
        /// </summary>
        [Obsolete("尚未实现此方法。", true)]
        public string UserName
        {
            get { throw new NotImplementedException(); }

            set { throw new NotImplementedException(); }
        }
    }
}