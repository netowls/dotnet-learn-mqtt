﻿namespace NetOwls.Tutorials.Mqtt.Common
{
    /// <summary>
    ///     提供了基本的 MQTT 服务连接凭据。
    /// </summary>
    /// <seealso>
    ///     <cref>IMqttConnectionCredentials</cref>
    /// </seealso>
    public class BasedConnectionCredentials : IMqttConnectionCredentials
    {
        /// <summary>
        ///     设置或获取登录口令。
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     设置或获取用户名。
        /// </summary>
        public string UserName { get; set; }
    }
}