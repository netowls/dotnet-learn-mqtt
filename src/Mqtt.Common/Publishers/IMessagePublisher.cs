﻿namespace NetOwls.Tutorials.Mqtt.Common.Publishers
{
    using System.Threading.Tasks;

    /// <summary>
    ///     定义了使用 MQTT 服务发布消息的接口。
    /// </summary>
    /// <seealso>
    ///     <cref>Common.IMqttConnection</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>System.Threading.Tasks.Task</cref>
    /// </seealso>
    public interface IMessagePublisher
    {
        /// <summary>
        ///     获取 MQTT 服务连接对象。
        /// </summary>
        IMqttConnection Connection { get; }

        /// <summary>
        ///     发布消息。
        /// </summary>
        /// <param name="message">
        ///     消息内容。
        /// </param>
        /// <returns>
        ///     <see>
        ///         <cref>Task</cref>
        ///     </see>
        ///     类型的对象实例。
        /// </returns>
        /// <seealso>
        ///     <cref>System.Threading.Tasks.Task</cref>
        /// </seealso>
        Task PublishAsync(string message);
    }
}