﻿namespace NetOwls.Tutorials.Mqtt.Common.Publishers
{
    using System;

    /// <summary>
    ///     为
    ///     <see>
    ///         <cref>System.Console</cref>
    ///     </see>
    ///     提供的消息发布辅助性方法。
    /// </summary>
    public static class ConsoleHelper
    {
        /// <summary>
        ///     等待输入直到退出信号。
        /// </summary>
        /// <param name="notExit">
        ///     当控制台中未输入退出信号 (英文字符 X) 时，执行的方法。
        /// </param>
        /// <seealso>
        ///     <cref>System.Action{T}</cref>
        /// </seealso>
        public static void WaitEnter(Action<string> notExit)
        {
            var currentSign = "Wait";
            while (!string.IsNullOrWhiteSpace(currentSign) && currentSign.ToLower() != "x")
            {
                Console.Write("请输入需要发布的消息：");
                currentSign = Console.ReadLine();
                notExit?.Invoke(currentSign);
            }
        }
    }
}