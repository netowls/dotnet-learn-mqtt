﻿namespace NetOwls.Tutorials.Mqtt.Common
{
    using System.Threading.Tasks;
    using MQTTnet.Client;

    /// <summary>
    ///     定义了管理 MQTT 服务连接的接口。
    /// </summary>
    /// <seealso>
    ///     <cref>System.Threading.Tasks.Task</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>MQTTnet.Client.IMqttClient</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>IMqttConnectionCredentials</cref>
    /// </seealso>
    public interface IMqttConnection
    {
        /// <summary>
        ///     获取客户端与 MQTT 服务的连接对象。
        ///     <para>
        ///         实现了
        ///         <see>
        ///             <cref>MQTTnet.Client.IMqttClient</cref>
        ///         </see>
        ///         接口的对象实例。
        ///     </para>
        /// </summary>
        /// <seealso>
        ///     <cref>MQTTnet.Client.IMqttClient</cref>
        /// </seealso>
        IMqttClient Client { get; }

        /// <summary>
        ///     获取 MQTT 服务连接所需的凭据信息。
        ///     <para>
        ///         实现了
        ///         <see>
        ///             <cref>IMqttConnectionCredentials</cref>
        ///         </see>
        ///         接口的对象实例。
        ///     </para>
        /// </summary>
        /// <seealso>
        ///     <cref>IMqttConnectionCredentials</cref>
        /// </seealso>
        IMqttConnectionCredentials Credentials { get; }

        /// <summary>
        ///     获取 MQTT 服务 IP 地址或名称。
        /// </summary>
        string ServerIPAddress { get; }

        /// <summary>
        ///     获取 MQTT 服务端口号。
        /// </summary>
        int ServerPortNumber { get; }

        /// <summary>
        ///     尝试异步连接 MQTT 服务。
        /// </summary>
        /// <returns>
        ///     <see>
        ///         <cref>System.Threading.Tasks.Task</cref>
        ///     </see>
        ///     类型的对象实例。
        /// </returns>
        /// <seealso>
        ///     <cref>System.Threading.Tasks.Task</cref>
        /// </seealso>
        Task ConnectAsync();

        /// <summary>
        ///     尝试异步断开 MQTT 服务的连接。
        /// </summary>
        /// <returns>
        ///     <see>
        ///         <cref>System.Threading.Tasks.Task</cref>
        ///     </see>
        ///     类型的对象实例。
        /// </returns>
        /// <seealso>
        ///     <cref>System.Threading.Tasks.Task</cref>
        /// </seealso>
        Task DisconnectAsync();
    }
}