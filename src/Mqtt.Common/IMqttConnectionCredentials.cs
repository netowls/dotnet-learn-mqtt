﻿namespace NetOwls.Tutorials.Mqtt.Common
{
    /// <summary>
    ///     定义了连接 MQTT 服务凭据接口。
    /// </summary>
    public interface IMqttConnectionCredentials
    {
        /// <summary>
        ///     设置或获取登录口令。
        /// </summary>
        string Password { get; set; }

        /// <summary>
        ///     设置或获取用户名。
        /// </summary>
        string UserName { get; set; }
    }
}