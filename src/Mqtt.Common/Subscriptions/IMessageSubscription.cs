﻿namespace NetOwls.Tutorials.Mqtt.Common.Subscriptions
{
    using System.Threading.Tasks;

    /// <summary>
    ///     定义了 MQTT 消息订阅接口。
    /// </summary>
    /// <seealso>
    ///     <cref>Common.IMqttConnection</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>System.Threading.Tasks.Task</cref>
    /// </seealso>
    public interface IMessageSubscription
    {
        /// <summary>
        ///     获取 MQTT 服务连接对象。
        /// </summary>
        IMqttConnection Connection { get; }

        /// <summary>
        ///     确定从 MQTT 订阅消息。
        /// </summary>
        /// <returns>
        ///     <see>
        ///         <cref>Task</cref>
        ///     </see>
        ///     类型的对象实例。
        /// </returns>
        /// <seealso>
        ///     <cref>System.Threading.Tasks.Task</cref>
        /// </seealso>
        Task EnsureSubscribeAsync();
    }
}