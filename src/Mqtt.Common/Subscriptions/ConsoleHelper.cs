﻿namespace NetOwls.Tutorials.Mqtt.Common.Subscriptions
{
    using System;

    /// <summary>
    ///     为消息订阅端提供的
    ///     <see>
    ///         <cref>System.Console</cref>
    ///     </see>
    ///     类型的辅助方法。
    /// </summary>
    public static class ConsoleHelper
    {
        /// <summary>
        ///     等待并阻塞控制台程序。
        /// </summary>
        public static void WaitAndBlock()
        {
            Console.ReadLine();
        }
    }
}