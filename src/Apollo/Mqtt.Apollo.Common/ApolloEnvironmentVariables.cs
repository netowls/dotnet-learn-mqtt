﻿namespace NetOwls.Tutorials.Mqtt.Common.Apollo
{
    /// <summary>
    ///     Apache Apollo MQTT 服务环境变量。
    /// </summary>
    internal static class ApolloEnvironmentVariables
    {
        /// <summary>
        ///     Apollo 服务默认 TCP 端口号。
        /// </summary>
        internal const int DefaultApolloPortNumber = 61613;
    }
}