﻿namespace NetOwls.Tutorials.Mqtt.Publishers.Apollo
{
    using Common;
    using Common.Apollo;
    using Common.Publishers;

    internal class Program
    {
        private static void Main(string[] args)
        {
            ConsoleProgramOptions.Configure();

            var connection = new ApolloConnection();
            var publisher = new MessagePublisher(connection);

            ConsoleHelper.WaitEnter(async s => { await publisher.PublishAsync(s); });
        }
    }
}