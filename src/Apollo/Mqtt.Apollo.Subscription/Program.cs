﻿namespace NetOwls.Tutorials.Mqtt.Subscriptions.Apollo
{
    using System.Threading.Tasks;
    using Common;
    using Common.Apollo;
    using Common.Subscriptions;

    internal class Program
    {
        private static void Main(string[] args)
        {
            ConsoleProgramOptions.Configure();

            var connection = new ApolloConnection();

            var subscription = new SimpleMessageSubscription(connection);

            Task.Run(async () => { await subscription.EnsureSubscribeAsync(); });

            ConsoleHelper.WaitAndBlock();
        }
    }
}