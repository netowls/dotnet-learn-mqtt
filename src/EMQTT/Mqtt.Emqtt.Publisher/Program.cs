﻿namespace NetOwls.Tutorials.Mqtt.Publishers.Emqtt
{
    using System.Threading.Tasks;
    using Common;
    using Common.Emqtt;
    using Common.Publishers;

    internal class Program
    {
        private static void Main(string[] args)
        {
            ConsoleProgramOptions.Configure();

            var connection = new EmqttConnection();

            var publisher = new MessagePublisher(connection);

            ConsoleHelper.WaitEnter(s => { Task.Run(async () => { await publisher.PublishAsync(s); }); });
        }
    }
}