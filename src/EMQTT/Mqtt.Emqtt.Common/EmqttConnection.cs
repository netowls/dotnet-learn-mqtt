﻿namespace NetOwls.Tutorials.Mqtt.Common.Emqtt
{
    /// <summary>
    ///     提供了管理 EMQTT 连接的方法。
    ///     <para>
    ///         密闭的，不可从此类型继承。
    ///     </para>
    /// </summary>
    /// <seealso>
    ///     <cref>Common.MqttConnectionBase</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>Common.IMqttConnectionCredentials</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>Common.BasedConnectionCredentials</cref>
    /// </seealso>
    public sealed class EmqttConnection : MqttConnectionBase
    {
        /// <summary>
        ///     获取 MQTT 服务连接所需的凭据信息。
        ///     <para>
        ///         实现了
        ///         <see>
        ///             <cref>IMqttConnectionCredentials</cref>
        ///         </see>
        ///         接口的对象实例。
        ///     </para>
        /// </summary>
        /// <seealso>
        ///     <cref>IMqttConnectionCredentials</cref>
        /// </seealso>
        /// <seealso>
        ///     <cref>Common.BasedConnectionCredentials</cref>
        /// </seealso>
        public override IMqttConnectionCredentials Credentials =>
            new BasedConnectionCredentials {UserName = "admin", Password = "123456"};

        /// <summary>
        ///     获取 MQTT 服务 IP 地址或名称。
        /// </summary>
        public override string ServerIPAddress => "localhost";

        /// <summary>
        ///     获取 MQTT 服务端口号。
        /// </summary>
        public override int ServerPortNumber => 1883;
    }
}