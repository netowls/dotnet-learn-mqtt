﻿namespace NetOwls.Tutorials.Mqtt.Subscriptions.Emqtt
{
    using System.Threading.Tasks;
    using Common;
    using Common.Emqtt;
    using Common.Subscriptions;

    internal class Program
    {
        private static void Main(string[] args)
        {
            ConsoleProgramOptions.Configure();

            var connection = new EmqttConnection();

            Task.Run(async () =>
            {
                await connection.ConnectAsync();
                await new SimpleMessageSubscription(connection).EnsureSubscribeAsync();
            });

            ConsoleHelper.WaitAndBlock();
        }
    }
}